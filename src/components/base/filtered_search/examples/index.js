import FilteredSearchDefaultExample from './filtered_search.default.example.vue';

export default [
  {
    name: 'Filtered search',
    items: [
      {
        id: 'filtered-search',
        name: 'default',
        component: FilteredSearchDefaultExample,
      },
    ],
  },
];
