import description from './new_dropdown_item.md';

export default {
  description,
  bootstrapComponent: 'b-dropdown-item',
};
